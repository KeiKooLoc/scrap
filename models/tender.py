from sqlalchemy import Column, Integer, String

from . import Base


class Tender(Base):
    id = Column(Integer, primary_key=True)
    title = Column(String)
    price = Column(String)
    announcement_date = Column(String)
    purchase_id = Column(String)

    __tablename__ = "tenders"

    def __repr__(self):
        return "Tender {}. {}".format(self.id, self.title)

    def to_dict(self):
        return {"id": self.id,
                "title": self.title,
                "price": self.price,
                "announcement_date": self.announcement_date,
                "purchase_id": self.purchase_id}
