from sqlalchemy import create_engine, MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

engine = create_engine("sqlite:///database.sqlite")
Session = sessionmaker(bind=engine)
Base = declarative_base()
