from pprint import pprint
import time

import requests
from bs4 import BeautifulSoup

from models.tender import Tender
from models import Base, engine, Session


class TendersParser(object):
    def __init__(self):
        self.url = "https://www.dzo.com.ua/tenders/current"
        self.session = Session()

    def loop_all_pages(self):
        total_pages = self.total_pages()
        print("Total Pages: {}".format(total_pages))
        for page in range(1, total_pages):
            print("loop over {} page".format(page))
            resp = requests.get(self.url, params={"page": page})
            time.sleep(1)
            if resp.status_code == 200:
                self.parse_page(resp.text)
            else:
                print("Request to {}, page {} failed. Status code: {}"
                      "".format(self.url, page, resp.status_code))

    def total_pages(self):
        resp = requests.get(self.url, params={"page": 1})
        time.sleep(1)
        if resp.status_code == 200:
            html_soup = BeautifulSoup(resp.text, "html.parser")
            total_pages = html_soup.find(
                "div", class_="pages relative clear").find_all("a")[-1].text
            return int(total_pages)
        else:
            return 0

    def parse_page(self, response_text):
        html_soup = BeautifulSoup(response_text, "html.parser")
        tenders_containers = html_soup.find_all(
            "div", class_="hoverMove markerOpacity")
        for container in tenders_containers:
            # Get tender ID
            purchase_id = container.find(
                "div", class_="cd newtenderId").find_all("span")[1].text

            if self.session.query(Tender).filter_by(
                    purchase_id=purchase_id).first():
                print("Tender: {}. Already added.".format(purchase_id))
                continue

            tender = Tender()
            tender.purchase_id = purchase_id
            # Get tender title from tender div container
            tender.title = container.h2.a.text
            # Get tender price from tender div container
            tender.price = container.find(
                "div", class_="newkvaziName clear").find(
                "span", class_="value").text
            # Get tender announcement date
            tender.announcement_date = container.find(
                "div", class_="newauction").find_all("span")[1].text
            # Create raw
            self.session.add(tender)
            self.session.commit()
            print("Tender: {}. Successfully added.".format(purchase_id))
            # pprint(tender.to_dict())


if __name__ == "__main__":
    Base.metadata.create_all(engine)
    TendersParser().loop_all_pages()
